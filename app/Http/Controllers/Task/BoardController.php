<?php

namespace App\Http\Controllers\Task;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class BoardController extends Controller
{

    public function index()
    {
        return view('board');
    }

    public function create(Request $req)
    {
        $message = '';
        $req->validate([
            'name' => 'required|between:1,16384',
            'desc' => 'required|between:0,16384',
        ]);
        
        if ($req->id == null) {
            $url = "https://api.trello.com/1/boards/?name=$req->name&desc=$req->desc&key=$req->key&token=$req->token";
            $message = Http::post($url)->successful();
        } else {
            $url = "https://api.trello.com/1/boards/$req->id?name=$req->name&desc=$req->desc&key=$req->key&token=$req->token";
            $message = Http::put($url)->successful();
        }

        return response()->json([
            'message' => $message,
        ]);
    }

    public function list(Request $req)
    {
        $url = "https://api.trello.com/1/members/me/boards?key=$req->key&token=$req->token";
        return Http::get($url)->object();
    }

    public function update(Request $req)
    {
        $url = "https://api.trello.com/1/boards/$req->id?name=$req->name&desc=$req->desc&key=$req->key&token=$req->token";
        return Http::put($url);
    }
    public function delete(Request $req)
    {
        $url = "https://api.trello.com/1/boards/$req->id?key=$req->key&token=$req->token";
        return Http::delete($url);
    }
    public function viewlist(Request $req)
    {
        $url = "https://api.trello.com/1/boards/$req->id/lists?key=$req->key&token=$req->token";
        return Http::get($url)->object();
    }
    public function boardlist()
    {
        return view('boardlist');
    }
    public function cardlist(Request $req)
    {
        $url = "https://api.trello.com/1/lists/$req->id/cards?key=$req->key&token=$req->token";
        return Http::get($url)->object();
    }
    public function listcreate(Request $req)
    {
        $url = "https://api.trello.com/1/boards/$req->id/lists?name=$req->name&key=$req->key&token=$req->token";
        return Http::post($url)->object();
    }
    //
    public function cardcreate(Request $req)
    {
        $url = "https://api.trello.com/1/cards?idList=$req->id&name=$req->name&desc=$req->desc&key=$req->key&token=$req->token";
        return Http::post($url)->object();
    }
}
