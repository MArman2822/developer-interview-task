<?php

namespace App\Http\Controllers\Task;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class AuthorizationController extends Controller
{
    public function index()
    {
        return view('authorization');
    }

    public function connect(Request $req)
    {
        $url = "https://api.trello.com/1/members/me/?key=$req->key&token=$req->token";
        $data =  Http::get($url)->object();
        Session::put('organization', $data);
        return $data;
    }
}
