<?php

namespace App\Http\Controllers\Task;

use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Controller;
use App\Models\Task\Purchases;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    public function index()
    {
        return view('task1');
    }

    public function mockapi()
    {
        $request = Http::get('https://raw.githubusercontent.com/Bit-Code-Technologies/mockapi/main/purchase.json')->object();

        foreach ($request as $result) {
            $value = new Purchases();
            $duplicate = Purchases::where('product_name', $result->product_name)->first();
            if (empty($duplicate)) {
                $value->name = $result->name;
                $value->order_no = $result->order_no;
                $value->user_phone = $result->user_phone;
                $value->product_code = $result->product_code;
                $value->product_name = $result->product_name;
                $value->product_price = $result->product_price ?? 0.00;
                $value->purchase_quantity = $result->purchase_quantity ?? 0;
                $value->save();
            }
        }
        return Purchases::orderby('purchase_quantity', 'desc')->get();
    }
}
