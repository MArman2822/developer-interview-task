<div class="col-12">
    <div class="d-flex justify-content-center bg-success fw-bold fs-5">Board Create</div>
    <div class="row mt-1">
        <div class="col-1"></div>
        <div class="col-4">
            <div class="mb-3">
                <input class="form-control form-control-sm" id="bid" hidden>
                <label for="name" class="form-label" require>Name</label>
                <input type="text" value="" class="form-control form-control-sm" id="name">
            </div>
            <div class="mb-3">
                <label for="apisecret" class="form-label">Description</label>
                <textarea type="text" value="" class="form-control form-control-sm" id="description"></textarea>
            </div>
            <button type="button" id="submit" onclick="submit()" class="btn btn-sm btn-primary">Submit</button>
            <button type="button" id="clear" onclick="clear()" class="btn btn-sm btn-danger">Refresh</button>
        </div>
        <div class="col-7" id="row">
        </div>
    </div>
</div>


<!-- modal -->
<!-- <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Board List</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <label>Name</label>
                <input type="text" value="" class="form-control form-control-sm" id="name">
                <button type="button" id="submit" onclick="createlist()" class="btn btn-sm btn-primary mt-1">Submit</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> -->

<!--  -->
<script>
    keyy = '2a04da80140282f2d8cb50a09e637aeb';
    tokenn = '1929181273253c68a6dc5691d3ffa245bc30811997127c088984922ad65ce944';
    $(document).ready(function() {
        boardList();
    });

    function submit() {
        let id = ($("#bid").val()) == "" ? null : $("#bid").val();
        let name = $("#name").val();
        let description = $("#description").val();
        if (name) {
            $.ajax({
                type: "post",
                url: "{{route('admin.create')}}",
                data: {
                    id: id,
                    name: name,
                    desc: description,
                    key: keyy,
                    token: tokenn
                },
                success: function(data) {
                    if (data) {
                        alert("Success");
                        boardList();
                        clear();
                    } else {
                        alert("Something went wrong !!")
                    }
                }
            });
        }else alert("name field can't be empty !!")
    }

    function boardList() {
        $.ajax({
            type: "get",
            url: "{{route('admin.list')}}",
            data: {
                key: keyy,
                token: tokenn
            },
            success: function(data) {
                $("#board").remove();
                $("#row").append("<div class='row row-cols-1 row-cols-md-3 g-4' id='board'>");
                for (let i = 0; i < data.length; i++) {
                    $("#board").append("<div class='col' id='col-" + (i + 1) + "'></div>");
                    $("#col-" + (i + 1)).append("<div class='card h-100' id='card-" + (i + 1) + "'></div>");
                    $("#card-" + (i + 1)).append("<div class='card-body text-center' id='cardB-" + (i + 1) + "'></div>");
                    $("#cardB-" + (i + 1)).append("<button class='btn btn-sm btn-primary' onclick='getBoardWiseList(" + (i + 1) + ")' id='id-" + (i + 1) + "' data-status='" + data[i].id + "' data-desc='" + data[i].desc + "'>" + data[i].name + "</button>");
                    $("#card-" + (i + 1)).append("<div class='card-footer text-end' id='cardf-" + (i + 1) + "'></div>");
                    $("#cardf-" + (i + 1)).append("<button type='button' class='btn btn-sm btn-primary' onclick='viewBoardList(" + (i + 1) + ")' id='vid-" + (i + 1) + "' data-vid='" + data[i].id + "'>View List</button> <button class='btn btn-sm btn-danger ml-2' onclick='deleteBoard(" + (i + 1) + ")' id='did-" + (i + 1) + "' data-did='" + data[i].id + "'><i class='fa fa-trash'></i></button>");
                }
            }
        });
    }

    rowIdx = 0;

    function setData(data) {
        for (var i = 0; i < data.length; i++) {
            $('#boardtable').append(`<tr id="R${++rowIdx}">
                <td class="row-index text-left" id='name-${rowIdx}'}"></td>
                <td class="row-index text-left"  id='des-${rowIdx}'">${data[i].student_name}</td>
                <td class="row-index text-center" id=""><i class='fa fa-edit text-info' onclick="setForEdit(this)"></i></td>
            </tr>`);
        }
        rowIdx = 0;
    }

    function getBoardWiseList(id) {
        let bid = $("#id-" + id).attr('data-status');
        let bname = $("#id-" + id).text();
        let bdesc = $("#id-" + id).attr('data-desc');

        $("#bid").val(bid);
        $("#name").val(bname);
        $("#description").val(bdesc);
    }

    function deleteBoard(id) {
        if (confirm('Are you sure to delete???')) {
            let bid = $("#did-" + id).attr('data-did');
            $.ajax({
                type: "post",
                url: "{{route('admin.delete')}}",
                data: {
                    id: bid,
                    key: keyy,
                    token: tokenn
                },
                success: function(data) {
                    boardList();
                    clear();
                }
            });
        }
    }

    function viewBoardList(id) {
        let vid = $("#vid-" + id).attr('data-vid');
        $.ajax({
            type: "get",
            url: "{{route('admin.viewlist')}}",
            data: {
                id: vid,
                key: keyy,
                token: tokenn
            },
            success: function(data) {
                BoardList(data);
            }
        });
    }

    function BoardList(bdata) {
        $.ajax({
            type: "get",
            url: "{{route('admin.boardlist')}}",
            data: {},
            success: function(data) {
                $("#body").html(data);
                $("#board").remove();
                $("#row").append("<div class='row row-cols-1 row-cols-md-4 g-4' id='board'>");
                $("#idBoard").val(bdata[0].idBoard);
                for (let i = 0; i < bdata.length; i++) {
                    $("#board").append("<div class='col' id='col-" + (i + 1) + "'></div>");
                    $("#col-" + (i + 1)).append("<div class='card h-100' id='card-" + (i + 1) + "'></div>");
                    $("#card-" + (i + 1)).append("<div class='card-body text-center' id='cardB-" + (i + 1) + "'></div>");
                    $("#cardB-" + (i + 1)).append("<button class='btn btn-sm btn-primary' onclick='viewListwiseCard(" + (i + 1) + ")' id='cid-" + (i + 1) + "' data-list-id='" + bdata[i].id + "'>" + bdata[i].name + "</button>");
                    $("#card-" + (i + 1)).append("<div class='card-footer text-end' id='cardf-" + (i + 1) + "'></div>");
                    $("#cardf-" + (i + 1)).append("<button class='btn btn-sm btn-danger ml-2' data-bs-toggle='modal' data-bs-target='#exampleModal2' id='submit' data-list-id='" + bdata[i].id + "'><i class='fa fa-plus'></i></button>");
                }
            }
        });
    }

    function viewListwiseCard(id) {
        let cid = $("#cid-" + id).attr('data-list-id');
        console.log("cid : ", cid);
        $.ajax({
            type: "get",
            url: "{{route('admin.cardlist')}}",
            data: {
                id: cid,
                key: keyy,
                token: tokenn
            },
            success: function(data) {
                $("#crow").remove();
                $("#cardlist").remove();

                $("#list").append("<div class='row' id='crow'></div>");
                $("#list").append("<div class='row row-cols-1 row-cols-md-4 g-4' id='cardlist'>");
                $("#crow").append("<div class='col text-center' id='celem'></div>");
                $("#celem").append("<h1 class='col text-center' id='celem'>Card List</h1>");
                if (data.length > 0) {
                    for (let i = 0; i < data.length; i++) {
                        $("#cardlist").append("<div class='col' id='ccol-" + (i + 1) + "'></div>");
                        $("#ccol-" + (i + 1)).append("<div class='card h-100' id='ccard-" + (i + 1) + "'></div>");
                        $("#ccard-" + (i + 1)).append("<div class='card-body text-center' id='ccardB-" + (i + 1) + "'></div>");
                        $("#ccardB-" + (i + 1)).append("<button class='btn btn-sm btn-primary' onclick='viewListwiseCard(" + (i + 1) + ")' id='ccid-" + (i + 1) + "' data-list-cid='" + data[i].id + "'>" + data[i].name + "</button>");

                        // $("#ccardB-" + (i + 1)).append("<div class='card-footer text-end' id='cardff-" + (i + 1) + "'></div>");
                        // $("#cardff-" + (i + 1)).append("<button class='btn btn-sm btn-danger ml-2' onclick='showHiddenItem()' data-bs-toggle='modal' data-bs-target='#exampleModal2' data-list-id='" + data[i].idList + "'><i class='fa fa-plus'></i></button>");
                    }
                }
            }
        });
    }

    function clear() {
        $("#bid").val("");
        $("#name").val("");
        $("#description").val("");
    }
</script>