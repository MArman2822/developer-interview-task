<!-- <html>

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>
    <div class="row"> -->
<div class="col-4">
</div>
<div class="col-4 mt-1">
    <div class="mb-3">
        <label for="apikey" class="form-label">Api key</label>
        <input type="text" value="2a04da80140282f2d8cb50a09e637aeb" class="form-control form-control-sm" id="apikey">
    </div>
    <div class="mb-3">
        <label for="apisecret" class="form-label">Api Secret</label>
        <input type="text" value="1929181273253c68a6dc5691d3ffa245bc30811997127c088984922ad65ce944" class="form-control form-control-sm" id="apisecret">
    </div>
    <button type="button" id="authorize" onclick="authorize()" class="btn btn-sm btn-primary">Authorize</button>
</div>
<div class="col-4">
</div>
<!-- </div>
</body> -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script>
    function authorize() {
        let key = $("#apikey").val();
        let token = $("#apisecret").val();
        if (key) {
            if (token) {
                $.ajax({
                    type: "get",
                    url: "{{route('admin.connect')}}",
                    data: {
                        _token: "{{csrf_token()}}",
                        key: key,
                        token: token
                    },
                    success: function(data) {
                        console.log("Data : ", data);
                        var options = "";
                        options += "<option selected id='org' value='" + data.idOrganizations[0] + "' data-id=''>" + data.initials + "</option>";
                        $('#org').html(options).trigger('change');
                        alert("Success");
                        $("#apikey").val("");
                        $("#apisecret").val("");
                    }
                });
            } else {
                alert("Input Secrate Key");
            }
        } else {
            alert("Input API Key");
        }
    }
</script>

<!-- </html> -->