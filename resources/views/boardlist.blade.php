<div class="row">
    <div class="d-flex justify-content-between bg-success">
        <!-- <input type="text" value="" class="form-control form-control-sm" id="idBoard" hidden> -->
        <div class="p-2 fw-bold">Create List</div>
        <div class="p-2">
            <button class="btn btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal3">Create</button>
        </div>
    </div>
</div>
<div class="col-12 mt-1" id="row">
</div>
<div class="col-12" id="list">
</div>

<!-- modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Board List</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <label>Name</label>
                <input type="text" value="" class="form-control form-control-sm" id="name">
                <button type="button" id="submit" onclick="submit()" class="btn btn-sm btn-primary mt-1">Submit</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- modal 2 -->
<div class="modal fade" id="exampleModal2" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Board List</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <label>Name</label>
                <input type="text" value="" class="form-control form-control-sm" id="name2">
                <textarea type="text" value="" class="form-control form-control-sm mt-1" id="description2"></textarea>
                <button type="button" id="submit" onclick="save()" class="btn btn-sm btn-primary mt-1">Submit</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- modal 3 -->
<div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Board List</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <label>Name</label>
                <input type="text" value="" class="form-control form-control-sm" id="idBoard" hidden>
                <input type="text" value="" class="form-control form-control-sm" id="name3">
                <button type="button" id="submit" onclick="submit()" class="btn btn-sm btn-primary mt-1">Submit</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    function submit() {
        // let bid = $("#submit").attr('data-list-id');
        let bid = $("#idBoard").val();
        let name = $("#name3").val();

        if (name) {
            $.ajax({
                type: "post",
                url: "{{route('admin.listcreate')}}",
                data: {
                    id: bid,
                    name: name,
                    key: keyy,
                    token: tokenn
                },
                success: function(data) {
                    console.log("data : ", data);
                    // alert("Success")
                }
            });
        } else alert("name field is required !!");


    }

    function save() {
        let bid = $("#submit").attr('data-list-id');
        let name = $("#name2").val();
        let description = $("#description2").val();
        if (name) {
            $.ajax({
                type: "post",
                url: "{{route('admin.cardcreate')}}",
                data: {
                    id: bid,
                    name: name,
                    desc: description,
                    key: keyy,
                    token: tokenn
                },
                success: function(data) {
                    console.log("data : ", data);
                    // alert("Success")
                }
            });
        } else alert("name field is required !!");

    }

    // function showHiddenItem() {
    //     $("#description").show();
    // }

    // function hideitem() {
    //     $("#description").hide();
    // }
</script>