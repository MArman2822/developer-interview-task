<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.101.0">
    <title></title>

    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        {{-- <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css')}}"> --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" integrity="sha512-HK5fgLBL+xu6dm/Ii3z4xhlSUyZgTT9tuc/hSrtw6uzJOvgRr2a9jyxxT1ely+B+xFAmJKVSTbpM/CuL7qxO8w==" crossorigin="anonymous" />
    </head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Trello</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link active" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" onclick="getPurchasePage()">Purchases</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" onclick="getAuthorizationPage()">Authorization</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" onclick="getBoardPage()">Board</a>
                    </li>
                </ul>
                <div class="">
                    <select class="form-select" id="org">
                        <!-- <option selected>Open this select menu</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option> -->
                    </select>
                </div>
            </div>
        </div>
    </nav>
    <div class="class=container-fluid">
        <div class="row" id="body"></div>
    </div>
</body>
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.min.js" integrity="sha384-ODmDIVzN+pFdexxHEHFBQH3/9/vQ9uori45z4JjnFsRydbmQbmL5t1tQ0culUzyK" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"></script>
<script>
    function getPurchasePage() {
        $.ajax({
            type: "get",
            url: "{{route('admin.purchase')}}",
            data: {},
            success: function(data) {
                $("#body").html(data);
            }
        });
    }

    function getAuthorizationPage() {
        $.ajax({
            type: "get",
            url: "{{route('admin.authorization')}}",
            data: {},
            success: function(data) {
                $("#body").html(data);
            }
        });
    }

    function getBoardPage() {
        $.ajax({
            type: "get",
            url: "{{route('admin.board')}}",
            data: {},
            success: function(data) {
                $("#body").html();
                $("#body").html(data);
            }
        });
    }
</script>

</html>