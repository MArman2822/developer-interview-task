<!-- <html>

<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body> -->
<!-- <div class="mt-1"> -->
<div class="col-4">

</div>
<div class="col-4 text-center">
    <button type="button" id="btnGenerate" class="btn btn-sm btn-danger pull-right mt-1" onclick="generate()">Generate Report</button>
</div>
<div class="col-4">

</div>
<!-- </div> -->
<table class="table table-bordered mt-1" id="tab">
    <thead class="table-dark">
        <th>Product Name</th>
        <th>Customer Name</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Total</th>
    </thead>
    <tbody id="datatable">
    </tbody>
    <tfoot>
        <tr>
            <th class="text-end" colspan="2">Gross Total :: </th>
            <td id="tQty"></td>
            <td id="tTprice"></td>
            <td id="tGross"></td>
        </tr>
    </tfoot>
</table>
<!-- </body> -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<script>
    function generate() {
        $.ajax({
            type: "get",
            url: "{{route('admin.mockapi')}}",
            data: {},
            success: function(data) {
                $("#datatable").empty();
                setData(data);
            }
        });
    }

    function setData(data) {
        let tqty = 0;
        let tTprice = 0.00;
        let tGross = 0.00;
        // let length = $("#datatable").length;
        for (var i = 0; i < data.length; i++) {
            tqty += data[i].purchase_quantity;
            tTprice += data[i].product_price;
            tGross += data[i].purchase_quantity * data[i].product_price;
            $('#datatable').append(
                `<tr>
                <td class="row-index">${data[i].product_name}</td>
                <td class="row-index">${data[i].name}</td>
                <td class="row-index">${data[i].purchase_quantity}</td>
                <td class="row-index">${data[i].product_price}</td>
                <td class="row-index">${data[i].purchase_quantity*data[i].product_price}</td>
            </tr>`);
        }

        $("#tQty").text(tqty);
        $("#tTprice").text(tTprice);
        $("#tGross").text(tGross);
    }
</script>

<!-- </html> -->