<?php

use App\Http\Controllers\Task\AuthorizationController;
use App\Http\Controllers\Task\BoardController;
use App\Http\Controllers\Task\PurchaseController;

//
Route::controller(PurchaseController::class)->prefix('purchases')->name('admin.')->group(function () {
    Route::get('index', 'index')->name('purchase');
    Route::get('mockapi', 'mockapi')->name('mockapi');
});

//
Route::controller(AuthorizationController::class)->prefix('authorization')->name('admin.')->group(function () {
    Route::get('index', 'index')->name('authorization');
    Route::get('connect', 'connect')->name('connect');
});

//
Route::controller(BoardController::class)->prefix('board')->name('admin.')->group(function () {
    Route::get('index', 'index')->name('board');
    Route::post('create', 'create')->name('create');
    Route::get('list', 'list')->name('list');
    Route::post('update', 'update')->name('update');
    Route::post('delete', 'delete')->name('delete');
    Route::get('viewlist', 'viewlist')->name('viewlist');
    Route::get('boardlist', 'boardlist')->name('boardlist');
    Route::get('cardlist', 'cardlist')->name('cardlist');
    Route::post('listcreate', 'listcreate')->name('listcreate');
    Route::post('cardcreate', 'cardcreate')->name('cardcreate');
});
